transitions = {
    '0a': '1', '0b': '0',
    '1a': '2', '1b': '0',
    '2a': '2', '2b': '3',
    '3a': '4', '3b': '0',
    '4a': '2', '4b': '5',
    '5a': '1', '5b': '0',
}


def finite_automation_matcher(text):
    q = '0'
    count = 0

    for i in text:
        q = transitions[q + i]

        if q == '5':
            count += 1

    return count


class InvalidInput(RuntimeError):
    def __init__(self, message):
        self.message = message


if __name__ == '__main__':
    try:
        pattern = input('text: ')
        filtered = list(filter(lambda ch: ch != 'a' and ch != 'b', list(pattern)))
        if len(filtered) > 0:
            raise InvalidInput(pattern)

        print(f'Matched occurrences: {finite_automation_matcher(pattern)}')
    except InvalidInput as e:
        print('Incorrect input: ' + str(e.message))
        raise e
