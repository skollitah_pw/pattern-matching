import unittest

import main


class TestPatternMatching(unittest.TestCase):

    def test_found_pattern(self):
        self.assertEqual(main.finite_automation_matcher('aababbaaabab'), 2)
        self.assertEqual(main.finite_automation_matcher('aababaabab'), 2)
        self.assertEqual(main.finite_automation_matcher('aabab'), 1)
        self.assertEqual(main.finite_automation_matcher('aa'), 0)
        self.assertEqual(main.finite_automation_matcher('baabaa'), 0)


if __name__ == '__main__':
    unittest.main()
